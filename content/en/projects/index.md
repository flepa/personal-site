+++
title = "Personal projects"

type = "blog"
+++

# :stadium: [Stadium concept](https://gitlab.com/flepa/stadium-cg)

**Stadium** is a **Computer Graphics** project developed as an **Android app** with **OpenGLES**. It's just a **really** basic application that just shows a rotating stadium with a soccer field and, if the user taps two times the screen, the stadium starts dissecting and shows a *field exchange* between two fields (soccer and basketball). After that, the other field is showed and the user can repeat the same operations as before in a infinite loop. 
You can see a demo in this [video](https://drive.google.com/file/d/19GF8a_T9-JbGArmG79uIq_kfCKElojDp/view?usp=sharing).  
The code base is written in **Java** and the main Android Development Guidelines are respected.  
For other information, there is the project's [repository](https://gitlab.com/flepa/stadium-cg).

# :balance_scale: [Python Load Balancer](https://gitlab.com/flepa/python-load-balancer)

**Python Load Balancer** is a Python application and an **alpha version** of a **distributed system**. A sort of *backbone* for more complex scenarios. The whole system consists in a **centralized authority** (e.g. a Raspberry Pi) which *can distribute* work in a **fair** way among the available **worker nodes** (e.g. ESP32 boards), in the current **network**, thanks to a **load balancer**. The **load balancer** is a **Python algorithm**, the communication between the entities is made through the MQTT and HTTP protocols.  
The aim is to provide an interaction between users and the **fair load balancer** through a **web application**. With this **web interface**, users can **choose** a task that will be executed on the *available* worker nodes and then see its **execution**, in real time, on the *workers*.

# :male_detective: [Reservation-Ninja](https://github.com/mattiolato98/reservation-ninja)

This is a web application that I have developed with my fellow [Gabriele Mattioli](https://www.gabrielemattioli.com/). This project
was born due to Covid-19: our University started asking students to provide **classroom reservations** (that have to be booked **every morning**) 
in order to join **lectures**.  
Of course, as with any developer, our laziness was stronger than our sense of duty, then we created a **python script** which *daily* reserves, **autonomously**,
the given classrooms. Our **open source** soul guided us and we shared this tool through a web application developed in Django called 
[Reservation Ninja](https://reservation-ninja.herokuapp.com/), which now counts more than **60** registered users (obviously, no computer scientist :sweat_smile:). That's it.

# :computer: [Machine Learning Analysis](https://gitlab.com/flepa/machine-learning-analysis)

This university project analyzes the **CIFAR-10** dataset and also applies on it some machine learning methodologies based on samples classification.
In order to get a deeper view of the project, I suggest you to check the **GitLab** page.

# :fork_and_knife: [Spoon Corner](https://gitlab.com/flepa/spooncorner)

A prototype of a Django web application for restaurants. 
With Spoon Corner, you can manage the restaurant menu and the orders placed online by customers. 
Customers can review the different dishes and see their order history. 
There is also a *naive* implementation of a recommendation system.  
  

# :camera: [PhotoAlbum](https://gitlab.com/flepa/photoalbum)

PhotoAlbum is a Java offline application. It manages a local photo album that contains different
galleries which are made of photos. Galleries can be protected with a password. Photos can be 
displayed individually or through a random slideshow.

# :unlock: [PDF-Unsecured](https://gitlab.com/flepa/pdf-unsecured)

This is a simple Python script which removes a password set to a collection of PDFs. 
A typical use-case scenario is to remove the password from the protected material of a University course.