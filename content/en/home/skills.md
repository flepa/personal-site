+++
# A Skills section created with the Featurette widget.
widget = "bars"  # See https://sourcethemes.com/academic/docs/page-builder/
weight = 20  # Order that this section will appear.

# Uncomment the following line and widget will NOT be displayed
# hidden = true

title = "Skills"
subtitle = "I am really good at the following technical skills."

[[bar]]
	icon = "/icons/python.png"
	name = "Python"
	percent = "85%"

[[bar]]
	icon = "/icons/c.png"
	name = "C"
	percent = "75%"

[[bar]]
	icon = "/icons/server.png"
	name = "SQL"
	percent = "70%"

[[bar]]
	icon = "/icons/java.png"
	name = "Java"
	percent = "65%"

[[bar]]
	icon = "/icons/javascript.png"
	name = "Javascript"
	percent = "65%"
+++
