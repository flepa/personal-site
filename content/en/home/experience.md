+++
widget = "timeline"
weight = 30  # Order that this section will appear.

# Uncomment the following line and widget will NOT be displayed
# hidden = true

# Widget title
title = "Experience"
# Widget subtitle
subtitle = "What lead me to acquire experience."

date_format = "Jan 2006" # Date format https://gohugo.io/functions/dateformat/#readout

[[period]]
  title = "Minerva Systems"
  subtitle = "Embedded Software Engineer"
  location = "Modena"
  date_start = "2023-01-01"
  date_end = ""
  description = "Startup in the automotive field."

[[period]]
  title = "Master's Degree"
  subtitle = "Computer Science @ Unimore"
  location = "Modena"
  date_start = "2020-09-01"
  date_end = "2022-12-16"
  description = "A forced choice to complete my studies."

[[period]]
  title = "Freelancer"
  subtitle = "Backend Engineer"
  location = "Modena"
  date_start = "2021-08-01"
  date_end = "2022-08-01"
  description = "During my Master's Degree course, I worked for a local enterprise."

[[period]]
  title = "Bachelor's Degree"
  subtitle = "Computer Science [Unimore]"
  location = "Modena"
  date_start = "2017-09-01"
  date_end = "2020-12-16"
  description = "My first University adventure."

[[period]]
  title = "High School"
  subtitle = "IT and Telecommunications [ITIS E. Fermi]"
  location = "Modena"
  date_start = "2012-09-01"
  date_end = "2017-07-01"
  description = "I started my IT career here."
+++
