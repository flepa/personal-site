+++
widget = "hero"
# Order that this section will appear.
weight = 10

# Uncomment the following line and widget will NOT be displayed
# hidden = true

# Hero image (optional). Enter filename of an image in the `static/` folder.
hero_media = "https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,w_514/v1628345901/sito_personale/1628259596799-min_jyhlvs.jpg"

# Buttons
# Add as much button as you want
[[btn]]
	# The url of the button
  url = "/about"
	# The text of the button
  label = "Know more"
	# Primary color
	primary = true

[[btn]]
  url = "#contact"
  label = 'Contact me'

+++

# Welcome!

Hi, I'm Filippo and this is my website. 
It's an environment to keep track of my personal and professional growth path. I hope you enjoy it!
