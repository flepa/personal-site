+++
title = "About me"

type = "blog"
+++

I am {{% years-till-today 1998 %}} years old and currently a Computer Science (Master's Degree) **fresh graduate**.

I started working as an **embedded software engineer** at [**Minerva Systems**](https://minervasys.tech/) in January 2023, and we are doing and plan to do big things in the automotive industry. Currently I'm working on the **NuttX RTOS**, the **Bao Hypervisor** and the **RISC-V ISA** with the goal of having an **impact** in the **autonomous drones field**.

I used to be a **freelancer** in my free time, it was a way to put into practice what I studied and to gain valuable experience.  
Furthermore, I did different **projects**, you can find theme on my  [**GitLab**](https://gitlab.com/users/flepa/projects) 
page.  

>I am on a continuous **journey** to discover new **passions** and grow the old ones 
>as well as to **evolve** as a person, both on a **relationship** and **professional** level.

![*My Master's Degree day.*](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,h_600/v1673184927/sito_personale/graduation_day_tazuab.jpg)


I'm in love with **Nature**. **Traveling**, **reading** and **hiking** are my **greatest passions**.

![*An incredible view from Seceda mountain (Italy).*](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,q_100,w_676/v1628352984/sito_personale/1628352742864-min_onxcj3.jpg)

Oh, I'm also a **guitar player** :guitar: and a **bonsai** keeper :deciduous_tree:.

| ![](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,h_400,q_100/v1628352983/sito_personale/1628351553755-min_aot1kr.jpg) | ![](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,h_400,q_100/v1628354406/sito_personale/1628354336545-min_b87v1r.jpg) | ![](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,h_400,q_100/v1628521454/sito_personale/1628521271832_hgco1u.jpg) |
| ------------- | ------------- | ------------- |

In case, here there is also my [**resume**](https://drive.google.com/file/d/1Uet66qRavnhSF7ZLpfpRzo8r5lgH-9LC/view?usp=sharing).  

That's it!
