+++
widget = "hero"
# Order that this section will appear.
weight = 10

# Uncomment the following line and widget will NOT be displayed
# hidden = true

# Hero image (optional). Enter filename of an image in the `static/` folder.
hero_media = "https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,w_514/v1628345901/sito_personale/1628259596799-min_jyhlvs.jpg"

# Buttons
# Add as much button as you want
[[btn]]
	# The url of the button
  url = "/about"
	# The text of the button
  label = "Scopri di più"
	# Primary color
	primary = true

[[btn]]
  url = "#contact"
  label = 'Contattami'

+++

# Benvenuto!

Ciao, sono Filippo e questo è il mio sito.
È uno spazio in cui racconto il mio percorso di crescita personale e professionale, spero sia di tuo gradimento!
