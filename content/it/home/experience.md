+++
widget = "timeline"
weight = 30  # Order that this section will appear.

# Uncomment the following line and widget will NOT be displayed
# hidden = true

# Widget title
title = "Esperienza"
# Widget subtitle
subtitle = "Cosa mi ha reso quello che sono."

date_format = "Jan 2006" # Date format https://gohugo.io/functions/dateformat/#readout

[[period]]
  title = "Minerva Systems"
  subtitle = "Embedded Software Engineer"
  location = "Modena"
  date_start = "2023-01-01"
  date_end = ""
  description = "Startup del settore automotive."

[[period]]
  title = "Laurea Magistrale"
  subtitle = "Informatica @ Unimore"
  location = "Modena"
  date_start = "2020-09-01"
  date_end = "2022-12-16"
  description = "Una scelta obbligata per completare i miei studi."

[[period]]
  title = "Freelancer"
  subtitle = "Backend Engineer"
  location = "Modena"
  date_start = "2021-08-01"
  date_end = "2022-08-01"
  description = "Durante il mio corso di laurea ho lavorato per un'azienda locale."

[[period]]
  title = "Laurea Triennale"
  subtitle = "Informatica [Unimore]"
  location = "Modena"
  date_start = "2017-09-01"
  date_end = "2020-12-16"
  description = "La mia prima avventura universitaria."

[[period]]
  title = "Scuola Superiore"
  subtitle = "Informatica e Telecomunicaizoni [ITIS E. Fermi]"
  location = "Modena"
  date_start = "2012-09-01"
  date_end = "2017-07-01"
  description = "Da qui comincia la mia carriera nel mondo IT."
+++
