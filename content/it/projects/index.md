+++
title = "Progetti personali"

type = "blog"
+++

# :stadium: [Stadium concept](https://gitlab.com/flepa/stadium-cg)

**Stadium** è un progetto di **Computer Grafica** sviluppato come una **app Android** con **OpenGLES**. È un'applicazione **molto** semplice che mostra uno stadio rotante con un campo da calcio e, se l'utente tocca due volte lo schermo, lo stadio inizia a sezionarsi ed a mostrare uno *scambio di campo* tra due campi (calcio e basket). Dopodiché l'altro campo è mostratp e l'utente può ripetere le stesse operazioni di prima in un ciclo infinito. È possibile vedere una demo in questo [video](https://drive.google.com/file/d/19GF8a_T9-JbGArmG79uIq_kfCKElojDp/view?usp=sharing).  
Il codice è scritto in **Java** e le principali Android Development Guidelines sono rispettate.  
Per altre informazioni, c'è il [repository](https://gitlab.com/flepa/stadium-cg) del progetto.

# :balance_scale: [Python Load Balancer](https://gitlab.com/flepa/python-load-balancer)

**Python Load Balancer** è un'applicazione Python che rappresenta una **versione alfa** di un **sistema distribuito**. Una sorta di *scheletro* per scenari più complessi. Il sistema consiste in una **autorità centrake** (e.g. un Raspberry Pi) che *può distribuire* lavoro **equamente** tra i **worker node** disponibili (e.g. board ESP32), nella **rete** attuale, grazie ad un **load balancer**. Il **load balancer** è un **algoritmo** sviluppato in **Python**, la comunicazione tra le entità è gestita tramite i protocolli MQTT e HTTP.  
L'obiettivo è quello di fornire un'interazione tra gli utenti ed il **load balancer** *equo* attraverso una **applicazione web**. Con questa **interfaccia web**, gli utenti possono **scegliere** un task che sarà eseguito sui worker node *disponibili* e poi potranno vedere la sua **esecuzione**, in tempo reale, sui *worker*.

# :male_detective: [Reservation-Ninja](https://github.com/mattiolato98/reservation-ninja)

Questa è un'applicazione web che ho sviluppato insieme al mio collega [Gabriele Mattioli](https://www.gabrielemattioli.com/). Questo
progetto è nato a causa del Covid-19: la nostra Università ha iniziato a chiedere agli studenti di fornire **prenotazioni per le aule** (che vanno 
prenotate **ogni mattina**) in modo da partecipare alle **lezioni**.
Ovviamente, come per ogni developer, la nostra pigrizia è stata più forte del nostro senso del dovere, dunque abbiamo creato uno **script python**
che *quotidianamente* prenota, in modo **automatico**, le classi specificate. La nostra anima **open source** ci ha spinti a condividere questo strumento
attraverso un'applicazione web sviluppata in Django chiamata [Reservation Ninja](https://reservation-ninja.herokuapp.com/), che ora conta più di **60** utenti registrati (nessun informatico, ovviamente :sweat_smile:).
Tutto qui.

# :computer: [Machine Learning Analysis](https://gitlab.com/flepa/machine-learning-analysis)

Questo progetto universitario analizza il dataset **CIFAR-10** e applica su di esso anche alcune metodologie di apprendimento automatico basate sulla classificazione di campioni.
Per avere una visione più approfondita del progetto, ti consiglio di controllare la pagina di **GitLab**.

# :fork_and_knife: [Spoon Corner](https://gitlab.com/flepa/spooncorner)

Un prototipo di applicazione web per ristoranti sviluppato in django.
Tramite Spoon Corner, puoi gestire il menù del ristorante e gli ordini generati online dai clienti.
I clienti possono recensire i piatti ordinati e visionare lo storico degli ordini.
È presente anche un'implementazione *naive* di un recommendation system.

# :camera: [PhotoAlbum](https://gitlab.com/flepa/photoalbum)

PhotoAlbum è un'applicazione Java offline. Gestisce un album fotografico locale che contiene diverse 
gallerie costituite da foto. Le gallerie possono essere protette da una password. Le foto possono essere
mostrate individualmente o attraverso uno slideshow casuale.

# :unlock: [PDF-Unsecured](https://gitlab.com/flepa/pdf-unsecured)

Questo è un semplice script in Python che rimuove una password impostata su di un insieme di PDF.
Uno scenario di utilizzo tipico è quello di rimuovere la password dal materiale protetto di un corso
universitario.