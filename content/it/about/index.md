+++
title = "Chi sono"

type = "blog"
+++

Ho {{% years-till-today 1998 %}} anni e attualmente sono un **neolaureato** Magistrale in Informatica. 

Da gennaio 2023 ho iniziato a lavorare come **embedded software engineer** presso [**Minerva Systems**](https://minervasys.tech/): abbiamo grandi piani per il futuro del settore **automotive**. Attualmente sto lavorando sul sistema operativo real-time **NuttX**, l'**Hypervisor BAO** a l'**ISA RISC-V** con l'obiettivo di avere un impatto nel campo dei **droni autonomi**.

Sono stato uno **sviluppatore libero professsionista** nel mio tempo libero, è stato un modo per tradurre 
nella pratica quello che ho studiato e per fare esperienza nell'industria del software.  
Inoltre, ho fatto diversi **progetti**, puoi trovarli sulla mia pagina [**GitLab**](https://gitlab.com/users/flepa/projects) personale.  

>Sono in un costante viaggio alla scoperta di ciò che può **appassionarmi** e farmi **evolvere** come persona, 
>sia a livello **relazionale** che **professionale**.

![*Il giorno della mia Laurea Magistrale.*](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,h_600/v1673184927/sito_personale/graduation_day_tazuab.jpg)

Amo la **Natura**. **Viaggiare**, **leggere** e fare **hiking** sono le mie più **grandi passioni**.

![*Una vista incredibile dal monte Seceda (Italia).*](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,q_100,w_676/v1628352984/sito_personale/1628352742864-min_onxcj3.jpg)

Ah, suono anche la **chitarra** :guitar: e accudisco un fantastico **bonsai** :deciduous_tree:.

| ![](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,h_400,q_100/v1628352983/sito_personale/1628351553755-min_aot1kr.jpg) | ![](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,h_400,q_100/v1628354406/sito_personale/1628354336545-min_b87v1r.jpg) | ![](https://res.cloudinary.com/dag3t3qu8/image/upload/c_scale,h_400,q_100/v1628521454/sito_personale/1628521271832_hgco1u.jpg) |
| ------------- | ------------- | ------------- |

Nel caso, ecco anche il mio [**curriculum**](https://drive.google.com/file/d/1Uet66qRavnhSF7ZLpfpRzo8r5lgH-9LC/view?usp=sharing).  

Ecco tutto!
