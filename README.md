# Welcome to my personal website!

Hi fellow traveler, this is the code of my [static website](https://www.filippofontana.eu/).

### Credits

Hugo example site for [Pico](https://github.com/negrel/hugo-theme-pico/) theme. 
See [instructions](https://github.com/negrel/hugo-theme-pico/).

---

2023